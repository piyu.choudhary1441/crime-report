import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from "../services/user.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  // variables
  registrationForm: FormGroup;
  payload: any = {}

  constructor(private fb: FormBuilder, private router: Router, private userService: UserService) {
    this.registrationForm = this.fb.group({
      firstName: new FormControl('', Validators.required),
      familyName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      confirmPassword: new FormControl('', Validators.required),
      userType: new FormControl('', Validators.required),
      contactNo: new FormControl('', Validators.required),
    });
   }

  ngOnInit(): void {
    this.getUserList();
  }
  goToRegister(){
    console.log('this is register information', this.registrationForm.value);
    this.createUser();
    // this.router.navigate(["../login"]);
  }

  getUserList(){
      this.userService.getUserList().subscribe( data => {
        if( data ){
          console.log(data);
        }
      })
  }

  createUser(){

    this.payload = 
      {
        id: null,
        given_name: this.registrationForm.value.firstName,
        family_name: this.registrationForm.value.familyName,
        email: this.registrationForm.value.email,
        contact_no: this.registrationForm.value.contactNo,
        user_type: this.registrationForm.value.userType,
        password: this.registrationForm.value.password
    }

    console.log('payload', this.payload);
    this.userService.createUser(this.payload).subscribe( data => {
      if( data ){
        console.log('data', data)
        alert('User Created Successfully');
        this.router.navigate(["../login"]);
      }
    })
  }

}
