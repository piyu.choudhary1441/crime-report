import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ComplaintsService } from '../services/complaints.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

  // variables
  reports: any[] = [];
  user_id: any;
  latitude: any;
  longitude: any;


  constructor(private router: Router, private route: ActivatedRoute, private compliantService: ComplaintsService, private userServices: UserService) { }

  ngOnInit(): void {
    this.getComplaits();
    
  }

  goToInfo(id){
    console.log('id', id);
    this.router.navigate(['../report-details'], {queryParams: {id : id , user_id: this.user_id}, relativeTo: this.route})
  }




  getComplaits(){
    this.compliantService.getComplaintsList().subscribe( data => {
     
      if( data && data.status){
        this.reports = data.data;
        console.log('report', this.reports);
        this.user_id = this.reports[0].user_id; 
        // this.getUserById();
      }

    })
  }


}
