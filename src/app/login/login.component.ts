import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  // variable 
  loginForm: FormGroup;
  userList: any = [];
  payload: any = {};

  constructor(private fb: FormBuilder, private router: Router, private userService: UserService, private activateRoute: ActivatedRoute) {
    this.loginForm = this.fb.group({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      userType: new FormControl('', Validators.required),
    });
   }

  ngOnInit(): void {
    this.getUserList();
  }
  getUserList(){
    this.userService.getUserList().subscribe( data => {
      if( data ){
        console.log('total user',data);
        this.userList = data;
      }
    })
  }
  goToLogin(){
    console.log('this is login info', this.loginForm.value); 
    localStorage.setItem('view', 'login');
    this.payload = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password,
      user_type: this.loginForm.value.userType
    }
    this.userService.userLogin(this.payload).subscribe( data => {
    
     if( data && data.status){
       data.data.forEach(user => {
        console.log(data);
          if( user.user_type === 'user'){
            this.router.navigate(['../dashboard'], { queryParams: { user_id : user.id },  relativeTo: this.activateRoute});
          }
          else if ( user.user_type  === 'admin'){
            this.router.navigate(['../admin-dashboard'], { queryParams: { user_id : user.id },  relativeTo: this.activateRoute});
          }
       });
     }
     else{
      alert('Enter Correct ID and Password...!');
      this.loginForm.reset();
     }
    })
      
  }
}
