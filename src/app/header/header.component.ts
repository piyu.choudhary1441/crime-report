import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  view: any;
  user_id: any;

  constructor(private router: Router, private route: ActivatedRoute) { 
    this.user_id = route.snapshot.queryParams.user_id
  }

  ngOnInit(): void {
    this.view = localStorage.getItem('view');
  }

  goToLogout(){
    this.view = 'logout';
    localStorage.setItem('view', 'logout');
    this.router.navigate(['../home']);
  }

  goToProfile(){
    this.router.navigate(['/profile'], { queryParams: { user_id: this.user_id}});
  }

}
