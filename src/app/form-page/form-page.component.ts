import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ComplaintsService } from '../services/complaints.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-form-page',
  templateUrl: './form-page.component.html',
  styleUrls: ['./form-page.component.css']
})
export class FormPageComponent implements OnInit {

  // variables
  questionAccidentFrom: FormGroup;
  crime_type: any;
  payload: any = {};
  user_id: any;
  constructor( private fb: FormBuilder, private router: Router, private activatedRoutes: ActivatedRoute, private complaintsService: ComplaintsService, private userServices: UserService) {
    this.crime_type = this.activatedRoutes.snapshot.queryParams.crime_type;
    this.user_id = this.activatedRoutes.snapshot.queryParams.user_id
    // this.queryParam = this.queryParam.toUpperCase();
    this.questionAccidentFrom = this.fb.group({
      date: new FormControl('', Validators.required),
      area: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      crimeType: new FormControl('accident', Validators.required),
      image: new FormControl('', Validators.required),
      longitude: new FormControl('', Validators.required),
      latitude: new FormControl('', Validators.required),
    });
   }

  ngOnInit(): void {
    this.getComplaints();
  }
  formSubmit(){
    console.log('typrOfCrime', this.crime_type);
    
    this.payload = {
      area: this.questionAccidentFrom.value.area,
      longitude: this.questionAccidentFrom.value.longitude,
      latitude: this.questionAccidentFrom.value.latitude,
      date: this.questionAccidentFrom.value.date,
      description: this.questionAccidentFrom.value.description,
      image: this.questionAccidentFrom.value.image,
      crime_type: this.crime_type,
      user_id: this.user_id
    }
    console.log('form value', this.payload);
    this.complaintsService.postComplaints(this.payload).subscribe( data => {
      console.log('data', data)
      if (data && data.status){
        alert('Successfully register report.... Thank You!!!')
        // redirect to dashboard
        this.router.navigate(['../dashboard'], {queryParams: {user_id: this.user_id}, relativeTo: this.activatedRoutes});
      }
    }, (error) => {
      console.log(error);
    })
    
  }

  getUserList(){
    this.userServices.getUserList()
  }

  getComplaints(){
    this.complaintsService.getComplaintsList().subscribe( data => {
      console.log(data);
    })
  }

  checkLatLong(){
    console.log(this.questionAccidentFrom.value.area);
    // if( navigator.geolocation){
    //   navigator.geolocation.getCurrentPosition((position: Position) => {
    //     this.questionAccidentFrom.controls.longitude.setValue(position.coords.longitude);
    //     this.questionAccidentFrom.controls.latitude.setValue(position.coords.latitude);
    //     // console.log('position', position.coords.latitude, position.coords.longitude);
    //   });
    // }

    
  }

}
