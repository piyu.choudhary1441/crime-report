import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ComplaintsService {

   getBaseUrl(){
    return 'http://localhost:1337/'
  }

  constructor(private http: HttpClient) { }

  getComplaintsList(): Observable<any> {
    // let headers = new HttpHeaders().set('Authorization')
    return this.http.get(this.getBaseUrl() + 'complaints');
  }

  postComplaints(data: any): Observable<any>{
    return this.http.post(this.getBaseUrl() + 'complaints/add', data, {responseType: 'json'});
  }

  getComplaintsById(id: any): Observable<any> {
    return this.http.get(this.getBaseUrl() + 'complaint/' + `${id}`);
  }
}
