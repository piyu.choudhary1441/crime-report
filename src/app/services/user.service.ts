import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  getBaseUrl(){
    return 'http://localhost:1337/'
  }
  constructor(private http: HttpClient) { }

  getUserList(): Observable<any> {
    // let headers = new HttpHeaders().set('Authorization')
    return this.http.get(this.getBaseUrl() + 'user/list');
  }

  createUser(data: any): Observable<any>{
    return this.http.post(this.getBaseUrl() + 'submit/userinfo', data, {responseType: 'json'});
  }

  userLogin(data: any): Observable<any>{
    
    return this.http.post(this.getBaseUrl() + 'login', data, {responseType: 'json'});
  }

  // get user by id
  userById(id: any): Observable<any> {
    return this.http.get(this.getBaseUrl() + 'user/' + `${id}`, {responseType: 'json'});
  }
}
