// <reference types="googlemaps" />
import { Component, OnInit, NgZone, ElementRef, Input, ViewChild, Output } from '@angular/core';
// import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { MapsAPILoader } from '@agm/core';
import { ActivatedRoute } from '@angular/router';
import { ComplaintsService } from '../services/complaints.service';
import { ThrowStmt } from '@angular/compiler';
// import { timeStamp } from 'console';



interface Coordinates {
  address: string;
  latitude: number;
  longitude: number;
}




@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.css']
})
export class GoogleMapComponent implements OnInit {

  @Input() lat ;
  @Input() long;

  title: string = 'AGM project';
  latitude: number = 21.3810;
  longitude: number = 79.7456;
  id: any;
  zoom: number;
  address: string;
  private geoCoder;
 
  @ViewChild('search')
  public searchElementRef: ElementRef;

  @Input() fromParent;
  agmMap: any;
 
  showMap: boolean = false;
 
  constructor(private activatedRoute: ActivatedRoute, 
    // private mapsAPILoader: MapsAPILoader,
    private apiloader: MapsAPILoader, private complaintsService: ComplaintsService
    // private ngZone: NgZone,
    // public activeModal: any
  ) {
    this.id = activatedRoute.snapshot.queryParams.id;
    // this.longitude = activatedRoute.snapshot.queryParams.longitude;
   }
 
 
  ngOnInit() {
    
    // this.getCurrentLocation();
    // this.get()  
    // // this.agmMap.triggerResize(true);  


    // //load Places Autocomplete
    // this.mapsAPILoader.load().then(() => {
    //   this.setCurrentLocation();
    //   this.geoCoder = new google.maps.Geocoder;
 
    //   let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
    //   autocomplete.addListener("place_changed", () => {
    //     this.ngZone.run(() => {
    //       //get the place result
    //       let place: google.maps.places.PlaceResult = autocomplete.getPlace();
 
    //       //verify result
    //       if (place.geometry === undefined || place.geometry === null) {
    //         return;
    //       }
 
    //       //set latitude, longitude and zoom
    //       this.latitude = place.geometry.location.lat();
    //       this.longitude = place.geometry.location.lng();
    //       console.log(this.latitude, 'latitude');
    //       this.zoom = 12;
    //     });
    //   });
    // });

    this.getReportById();
    this.lat = parseFloat(this.lat);
    this.long = parseFloat(this.long);
    console.log('lat long', this.lat, this.long);

    if ( this.lat && this.long != 0){
      this.showMap = true;
    }
    
  }
 
  // Get Current Location Coordinates
 

  getReportById(){
    this.complaintsService.getComplaintsById(this.id).subscribe( data => {
      if ( data && data.status){
        // console.log('reports', data.data)
        this.longitude = data.data[0].longitude;
        this.latitude = data.data[0].latitude;
        console.log(this.latitude, this.longitude);
      }
    })
  }
 
 
  

  
   
  // saveLocation(){
  //   const data = {
  //     address: this.address,
  //     latitude: this.latitude,
  //     longitude: this.longitude
  //   }
  // }
  // setCurrentLocation() {
  //   if ('geolocation' in navigator) {
  //     navigator.geolocation.getCurrentPosition((position) => {
  //       this.latitude = 27.2046;
  //       this.longitude = 77.4977;
  //       this.zoom = 8;
  //       console.log('location', this.latitude)
  //       this.getAddress(this.latitude, this.longitude);
  //     });
      
  //   }
  // }
  markerDragEnd($event: any) {
    console.log($event);
    this.latitude = $event.latLng.lat.name;
    this.longitude = $event.latLng.lng.name;
    console.log('new location', this.latitude, this.latitude);
    this.getAddress(this.latitude, this.longitude);
  }
 
  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
 
    });
  }
  
  // closeModal(sendData) { 
  //   // this.activeModal.close(sendData); 
  // }

  // getCurrentLocation(){
  //   console.log('navigator.geolocation', navigator.geolocation);
  //   navigator.geolocation.getCurrentPosition( (position: Position) => {
  //     // this.lat: ${position.coords.latitude};  
  //     // this.lng = position.coords.longitude;  
  //     console.log(`lat : ${position.coords.latitude}, long: ${position.coords.longitude}`);
  //   })
  // }

//   get() {  
//     if (navigator.geolocation) {  
//         navigator.geolocation.getCurrentPosition((position: Position) => {  
//             if (position) {  
//                 this.lat = position.coords.latitude;  
//                 this.lng = position.coords.longitude;  
//                 // this.getAddress = (this.lat, this.lng)  
//                 console.log(position)  
//                 this.apiloader.load().then(() => {  
//                     let geocoder = new google.maps.Geocoder;  
//                     let latlng = {  
//                         lat: this.lat,  
//                         lng: this.lng  
//                     };  
//                     geocoder.geocode({  
//                         'location': latlng  
//                     }, function(results) {  
//                         if (results[0]) {  
//                             this.currentLocation = results[0].formatted_address;  
//                             console.log(this.assgin);  
//                         } else {  
//                             console.log('Not found');  
//                         }  
//                     });  
//                 });  
//             }  
//         })  
//     }  
// } 
mapClicked($event) {  
  this.latitude = $event.coords.lat,  
      this.longitude = $event.coords.lng  
  this.apiloader.load().then(() => {  
      let geocoder = new google.maps.Geocoder;  
      let latlng = {  
          lat: this.latitude,  
          lng: this.longitude  
      };  
      geocoder.geocode({  
          'location': latlng  
      }, function(results) {  
          if (results[0]) {  
              this.currentLocation = results[0].formatted_address;  
              console.log(this.currentLocation);  
          } else {  
              console.log('Not found');  
          }  
      });  
  });  
}

  
}