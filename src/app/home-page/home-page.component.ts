import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  
  login: boolean = false;
  infoValueOf: any;
  zonalStation: any[] = [];
  zoneList: any[] = [
    { id: 'zone_1', name: 'Zone 1'},
    { id: 'zone_2', name: 'Zone 2'},
    { id: 'zone_3', name: 'Zone 3'},
    { id: 'zone_4', name: 'Zone 4'},
  ];
  stations: any[] = [
    { name: 'Snegaon', zone: 'zone_1'},{ name: 'BajajNagar', zone: 'zone_1'},{ name: 'Hingana', zone: 'zone_1'},{ name: 'Wadi',zone: 'zone_1'},{ name: 'RanaPratapnagar', zone: 'zone_1'},
    { name: 'Ambazari',zone: 'zone_2'},{ name: 'Sadar',zone: 'zone_2'},{ name: 'Dhantoli',zone: 'zone_2'},{ name: 'Mankapur',zone: 'zone_2'},{ name: 'Gittikhadan',zone: 'zone_2'},{ name: 'Sitabuldi',zone: 'zone_2'},
    { name: 'Panchpaoli', zone: 'zone_3'},{ name: 'Kotwali', zone: 'zone_3'},{ name: 'Lakadganj', zone: 'zone_3'},{ name: 'Tahsil', zone: 'zone_3'},{ name: 'Ganeshpeth', zone: 'zone_3'},{ name: 'Shanti Nagar', zone: 'zone_3'},
    { name: 'Hudkeshwar', zone: 'zone_4'},{ name: 'Ajani', zone: 'zone_4'},{ name: 'Imamwada', zone: 'zone_4'},{ name: 'Sakardara', zone: 'zone_4'},{ name: 'Nandanwan', zone: 'zone_4'},{ name: 'Wathoda', zone: 'zone_4'},{ name: 'Beltarodi', zone: 'zone_4'}, { name: 'Kalamana', zone: 'zone_4'}
  ];
  constructor(private router: Router, private route: ActivatedRoute,) {
    
   }

  ngOnInit(): void {
  }
  goToFormForAccident(){

    this.router.navigate(['../form'], {queryParams: {crime_type : 'accident'}, relativeTo: this.route});
  }
  goToFormForMurder(){
    this.router.navigate(['../form'], {queryParams: {crime_type : 'murder'}, relativeTo: this.route});
  }
  goToFormForRobbery(){
    this.router.navigate(['../form'], {queryParams: {crime_type : 'robbery'}, relativeTo: this.route});
  }
  goToFormForKidnaping(){
    this.router.navigate(['../form'], {queryParams: {crime_type : 'kidnaping'}, relativeTo: this.route});
  }
  goToFormForFights(){
    this.router.navigate(['../form'], {queryParams: {crime_type : 'fight'}, relativeTo: this.route});
  }
  goToFormForStolenVehicle(){
    this.router.navigate(['../form'], {queryParams: {crime_type : 'stolen-vehicle'}, relativeTo: this.route});
  }
  goToFormForMissingPerson(){
    this.router.navigate(['../form'], {queryParams: {crime_type : 'missing-person'}, relativeTo: this.route});
  }
  goToFormForOthers(){
    this.router.navigate(['../form'], {queryParams: {crime_type : 'others'}, relativeTo: this.route});
  }
  showInformation(event){
    this.zonalStation = [];
    console.log(event);
    this.infoValueOf = event;

    this.stations.forEach( element => {
     
      if( element.zone === event){
        console.log('element', element);
        this.zonalStation.push(element);
      }
    });
    console.log('this.zonalStation', this.zonalStation);

  }
}
