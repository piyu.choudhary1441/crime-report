import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home-page/home-page.component';
import { FormPageComponent } from './form-page/form-page.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserComponent } from './user/user/user.component';
import { UserHomeComponent } from './user-home/user-home.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { GoogleMapComponent } from './google-map/google-map.component';
import { CurrentlocationComponent } from './currentlocation/currentlocation.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { ReportDetailsComponent } from './report-details/report-details.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  { path: 'user', component: UserHomeComponent},
  { path: 'profile', component: ProfileComponent},
  { path: 'home' , component: HomePageComponent},
  { path: 'crime-report', component: FormPageComponent},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'dashboard', component: DashboardComponent},
  { path: 'map', component: GoogleMapComponent},
  { path: 'current-location', component: CurrentlocationComponent},
  { path: 'admin-dashboard', component: AdminDashboardComponent},
  { path: 'report-details', component: ReportDetailsComponent},
  { path: '', pathMatch: 'full', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
