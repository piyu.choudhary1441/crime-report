import { MapsAPILoader } from '@agm/core';
import { Component, ElementRef, Input, NgZone, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ComplaintsService } from '../services/complaints.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-report-details',
  templateUrl: './report-details.component.html',
  styleUrls: ['./report-details.component.css']
})
export class ReportDetailsComponent implements OnInit {

  report_id: any ;
  reportInfo: any[] = [];
  user_id: any;
  user: any[] = [];
  lat = 21.3808;
  lng = 79.7456;
  title: string = 'AGM project';
  latitude: number =  21.3808;
  longitude: number = 79.7456;
  zoom: number;
  address: string;
  private geoCoder;
  @ViewChild('search')
  public searchElementRef: ElementRef;
  
  @Input() fromParent;
  agmMap: any;
   
  showmap: boolean = false;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, private complaintService: ComplaintsService, private userService: UserService,  private mapsAPILoader: MapsAPILoader, private apiloader: MapsAPILoader, private ngZone: NgZone,) {
    this.report_id = activatedRoute.snapshot.queryParams.id;
    this.user_id = activatedRoute.snapshot.queryParams.user_id
   }

  ngOnInit(): void {
    this.getComplaintById();
    this.getUserById();
  }

  getComplaintById(){
    this.complaintService.getComplaintsById(this.report_id).subscribe( data => {
      if( data && data.status){
        this.showmap = true;
        console.log(data.data)
        this.reportInfo = data.data
        this.lat = this.reportInfo[0].latitude;
        this.lng = this.reportInfo[0].longitude;
      }
    })
  }

  getUserById(){
    // console.log(this.user_id);
    this.userService.userById(this.user_id).subscribe( data => {
      if( data && data.status){
        

        console.log(data);
        this.user = data.data;

      }
    })
  }

  mapStatus(event){
   console.log('event', event) 
  }
  

}
