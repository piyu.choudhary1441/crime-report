import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  view: any;
  user_id: any;
  userName: string;
  userEmail: any;
  userContactNo: any;
  constructor(private router: Router, private route: ActivatedRoute, private userService: UserService) {
    this.user_id = route.snapshot.queryParams.user_id;
   }

  ngOnInit(): void {
    this.view = localStorage.getItem('view');
    console.log(this.user_id)
    this.getUserInfo();
  }


  getUserInfo(){
    this.userService.userById(this.user_id).subscribe( data => {
      if( data && data.status){
        console.log('data', data)
        this.userName = data.data[0].given_name +' '+ data.data[0].family_name ;
        this.userEmail = data.data[0].email;
        this.userContactNo = data.data[0].contact_no;

      }
    })
  }

  showSubMenu(){
    const ele = document.querySelector<HTMLElement>('.sub_menu_report')
    if(ele.style.display == 'block'){
      ele.style.display = 'none';
    }
    else{
        ele.style.display = 'block';
    }
  }

  showSubMenuSettings(){
    const ele = document.querySelector<HTMLElement>('.sub_menu_setting');
    if(ele.style.display == 'block'){
      ele.style.display = 'none';
    } 
    else{
        ele.style.display = 'block';
    }
  }

  
  goToLogout(){
    this.view = 'logout';
    localStorage.setItem('view', 'logout');
    this.router.navigate(['../home']);
  }
}
